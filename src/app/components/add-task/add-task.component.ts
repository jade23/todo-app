import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from 'src/app/Task';
import { UiService } from 'src/app/services/ui.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask: EventEmitter<Task> = new EventEmitter();
  taskInput!: string;
  dateTime!: string;
  reminder: boolean = false;
  showAddTask: boolean = false;
  subscription: Subscription = new Subscription;

  constructor(private uiService: UiService) {
    this.subscription = this.uiService.onToggle().subscribe((value) => this.showAddTask = value);
  }

  ngOnInit(): void {
  }

  onSubmit(){
    if(!this.taskInput){
      alert('Please Input Task Name');
      return;
    }

    const newTask = {
      text: this.taskInput,
      day: this.dateTime,
      reminder: this.reminder,
    }

    this.onAddTask.emit(newTask);

    this.taskInput = '';
    this.dateTime = '';
    this.reminder = false;

  }

}
