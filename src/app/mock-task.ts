import {Task} from './Task';

export const TASKS: Task[] = [
  {
    id: 1,
    text: 'Meeting at School',
    day: 'May 6th at 1:30pm',
    reminder: true,
  },
  {
    id: 2,
    text: 'Going to Church',
    day: 'May 8th at 5:30pm',
    reminder: false,
  },
  {
    id: 3,
    text: 'Job Interview',
    day: 'June 22th at 10:00am',
    reminder: true,
  },
  {
    id: 4,
    text: 'Game',
    day: 'June 24th at 9:00am',
    reminder: false,
  },
];
